from django.conf.urls import url, include
from django.views.generic import ListView
from announcements.models import Announcement

urlpatterns = [
    url(r'^$', ListView.as_view(
        queryset=Announcement.objects.all().order_by("-date")[:25],
        template_name="announcements/announcements.html")),
]
