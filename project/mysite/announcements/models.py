from django.db import models

# Create your models here.
class Announcement(models.Model):
	content = models.CharField(max_length=200)
	date = models.DateTimeField()

	def __str__(self):
		return self.content