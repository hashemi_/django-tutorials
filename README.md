# Django Tutorial

This tutorial is intended for users who are new to coding and looking to further their knowledge in web development using python programming language. Python has many web development tools one of the most popular libraries is django

# Python Instillation
We will start this tutorial by making sure you have the latest python installed in your computer. If you are sure that you don't have python installed in your system you can go to the official [python software foundation]() website and check the download section.

# Virtual Environments
Once you have the latest python installed in your system, we need to create a virtual environment. What is a virtual environment? It is a way to isolate the work and the coding you will do from the rest of your system, some programs can run into conflict with other programs or when you are looking to update your code its a great way to isolate environments and see how they behave to run create an environment. We begin by creating a new folder to keep all our work by running the below command

```sh
$ mkdir project
```
Then we run the command
```sh
$ virtualenv venv
```
To activate the command in windows setting
```sh
$ source venv/scripts/activate
```
To activate in a mac environment
```sh
$ source bin/activate
```
To deactivate the enviroment
```sh
# To deactivate thre environments in windows
$ source venv/scripts/deactivate
# To deactivate thre environments in mac
$ source venv/bin/deactivate
```
# Installing Django
Now that we have a command and understanding of our virtual environment we install django by using pip run the below command making sure that our environment is activated
```sh
$ pip install django
```
 When django finished completing the work of installation you can move to the next step of creating your website by running the command
```sh
$ django-admin startproject mysite
```
You don't have to use the name mysite for this part the name can be anything you want it can be a title for your main site this will be the heart of the site. To make sure that you site is running perfect lets run
```sh
$ python manage.py runserver
```
You should be getting the following image

![](https://raw.githubusercontent.com/LambdaSchool/Hello-Django/master/success.png)


under your local host http://127.0.0.1:8000/  congratulations on reaching this far

lets build our first app by typing
```sh
$ python manage.py startapp webapp
```
Making sure that we are in the mysite folder this will create a new app, while looking at the app try to spot the files inside the two folders mysite and webapp. Once the file is created immedialtely change the following files start with mysite/setting.py to
```python
INSTALLED_APPS = [

'webapp',

'django.contrib.admin',

'django.contrib.auth',

'django.contrib.contenttypes',

'django.contrib.sessions',

'django.contrib.messages',

'django.contrib.staticfiles',

]
```
Add webapp as written above then we go and create a new python that was not there before called urls.py (mysite/urls.py) to add the home page and render the site
```python
from django.contrib import admin

from django.urls import path, include



urlpatterns = [

    path('admin/', admin.site.urls),

    path('', include('webapp.urls')),

]
```
Now we move to the interesting part of creating a site we start add to our webapp/views.py code

 ```python
from django.shortcuts import render

def index(request):
    return render(request, "webapp/home.html")
```

Then we create a folder inside our webapp called templates and inside this template folder we create another folder called webapp the reason for this is simply so when we render our site and our application becomes bigger jinja python render application do not get confuse with html templates that have the same name

The below template will be our home.html file
 ```html
{% extends "webapp/header.html" %}

{% block content %}

<p>Hey! Welcome to the first lesson in python django, you made it</p>

{% endblock %}
```
The below template will be our header.html file

 ```html
<!DOCTYPE html>

<html lang="en">



<head>

<title>Kuwait Python Programming  Meetup</title>

<meta charset="utf-8" />

</head>



<body class="body" style="background-color:#f6f6f6">

<div>

{% block content %}

{% endblock %}

</div>

</body>



</html>
```  
Back by popular demand the file structure of our last tutorial

![](http://gdurl.com/qOs6)

# Django Tutorial Part 2
Lesson two will start by further developing our webapp, by including a proper html page and adding bootstrap to the mix.

# What is Bootstrap?
A bit of history about bootstrap it was developed by twitter to make websites more dynamic and look better on small screens like phones and pads and it became very popular that twitter decided to open sourced the software and now its the number one open source project in the world.

The are two main ways to work bootstrap to your code either by CDN (Content Delivery Network) the code into your html like what we did in the webapp/home.html page  or you can actually download the file into your computer and add it to your new created file /project/mysite/webapp/static/webapp there is a css file and js file both need to be added

Now with django there are many ways to styles your sheet and it really depends on the design you like or what your client requested and/or what you like to see

More information can be found in https://getbootstrap.com/

# Add img file
Add the img file to the path project/mysite/webapp/static/webapp/img so it can render your images

# Contact Us Section
As you can see from the website there are two main sections blog and contact us we will now work on the contact us section with django there are many ways to link to that contact us page we can build it as an app in it self especially if it has a contact us form and fields that need to be filled in but in our case we will keep it simple and just render a simple page

We will start by heading to our file project/mysite/webapp/views.py
and add the following code

```python
from django.shortcuts import render

# Create your views here.

def index(request):
    return render(request, "webapp/home.html")

def contact(request):  
	return render(request,  'webapp/basic.html',{'content':['If you enjoy python join us send us a message @',
                               'http"//www.meetup.com/Kuwait-Python-Programming-Group/']})
```
After completing the code we need a way to inform django to render the site this can be achieved with little html as you can see from below

 ```html
{% extends "webapp/header.html" %}
{% block content %}
	{% for c in content%}
	   <p>{{c}}</p>
	{% endfor %}
{% endblock %}
```

Just before running the server we need to inform the place of the url to django we go to file project/mysite/webapp/urls.py and add the below line of code
```python
from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^contact/', views.contact, name='contact'),

]
```
We are done you can now run your server with command
```sh
$ python manage.py runserver
```
# Blog
To many the blog section of a website tend to be an app on its own and therefore we will start by writing the command
```sh
$ python manage.py startapp blog
```
What do we need to do after we complete every startapp command?

Two things

Go to project/mysite/mysite/setting.py to add the blog
```python
INSTALLED_APPS = [
'blog',

'webapp',

'django.contrib.admin',

'django.contrib.auth',

'django.contrib.contenttypes',

'django.contrib.sessions',

'django.contrib.messages',

'django.contrib.staticfiles',

]
```
Then go to the file project/mysite/mysite/urls.py and the specified path to the code  

```python
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('webapp.urls')),
    path('blog/', include('blog.urls')),
]
```
Once you have the blog setup you can now headover to your blog folder and edit the models file as follow
```python
from django.db import models

# Create your models here.

class Post(models.Model):
    title = models.CharField(max_length = 140)
    body = models.TextField()
    date = models.DateTimeField()

    def __str__(self):
        return self.title
```
As you can see we created this class with simple information that we need in a blog a title a body to keep the text and date we published

What do visitors expect when they visit the blog section of any site they expect to see a list of the blogs available to read and to achieve this django has already made views for us. ListView is one of the many views available to render. lets's start by making a new python file in

project/mysite/blog/urls.py

This code will work by calling on the views type and then moving to our models which we just made and displaying the order from new to old that why we use a negative sign in the date limited by only 25 displayed post and then finally we render an html template thats for rendering the list view next we need to move to the blog post itself and we render that by working with detail view

The question mark P is for capturing a primary key which is a digit to learn more

https://docs.djangoproject.com/en/2.1/topics/http/urls/   

```python
from django.conf.urls import url, include
from django.views.generic import ListView, DetailView
from blog.models import Post

urlpatterns = [
    url(r'^$', ListView.as_view(
        queryset=Post.objects.all().order_by("-date")[:25],
        template_name="blog/blog.html")),

    url(r'^(?P<pk>\d+)$', DetailView.as_view(
        model=Post,
        template_name="blog/post.html")),

]

```
To learn more about the available views in django checkout the documentation on the website

https://docs.djangoproject.com/en/2.1/ref/class-based-views/generic-display/

As we have seen from our previous lesson we need to build an html with the specified bath in our urls.py we start by creating a new template folder and inside this folder we create another folder called blog and inside the blog folder we provide the template the path for this template folder should be

project/mysite/blog/templates/blog/blog.html

 ```html
{% extends "webapp/header1.html" %}
{% block content %}
	{% for post in object_list %}
		<h5>{{ post.date|date:"Y-m-d" }}<a href="/blog/{{post.id}}">  {{ post.title }}</a></h5>
	{% endfor %}
{% endblock %}

```
project/mysite/blog/templates/blog/post.html

```html
{% extends "webapp/header1.html" %}
{% block content %}
<h3><a href="/blog/{{post.id}}">{{ post.title }}</a></h3>
<h6> on {{ post.date }}</h6>

<div class = "container">
	{{ post.body|safe|linebreaks}}
</div>
<br><br>
{% endblock %}


```


# SQL Database

Unless you are a fan of writing SQL, django takes care of all the database commands for you. You probably noticed the message with the start of your app that migrations need to be done on your pre-install application in the setting file we need to run the below command to move forward
```sh
$ python manage.py migrate
```
Notice how all the mentioned app have migrated but our blog has not moved the reason for that there is another command that we need to do in order for the new models to be created.
```sh
$ python manage.py makemigrations
```
Eveytime you create a new model you need to run the above command. Once the blog has been created we run the migrate command again
```sh
$ python manage.py migrate
```
To learn more about migrations please check the django documentation

https://docs.djangoproject.com/en/2.1/topics/migrations/

# Admin

Lets take a quick recap to where we are now on the site we learned about rendering content on the site then we moved to creating databases and done our blog section however now that we completed the blog section there is no content and we need to create some content that where admin comes in we start by creating a superuser by running the command  
```sh
$ python manage.py createsuperuser
```
Follow the instruction by providing your username, Email and password and very important don't forget to run the server before entering the password  

Once you created your superuser, now we really see how django magic work we write the code command in the file path project/mysite/blog/admin.py

```python
from django.contrib import admin
from blog.models import Post

# Register your models here.

admin.site.register(Post)
```
